import { useState, useEffect } from 'react';
import Banner from '../components/Banner';
import ProductCarousel from '../components/ProductCarousel';
import { Container, Spinner } from 'react-bootstrap';

export default function Home(){
	const [albums, setAlbums] = useState([]);
	const [loading, setLoading] = useState(true);
	
	const data = {
		title: 'Build your collection',
		subheader: 'Crate-digging made easy'
	}

	const getAlbumData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(res => res.json())
		.then(data => {
			const randomAlbumArr = data.slice(Math.max(data.length -3, 1))
			
			setAlbums(randomAlbumArr)
			setLoading(false)
		})
	}

	useEffect(() => {
		getAlbumData();

	}, [])

	return(
		<>
			<Banner bannerProps={data}/>
			<div className="mt-3">
				<h1 className="text-center">Featured Albums</h1>
				{(loading === true)
					? 	<Container className="text-center">
							<Spinner className="mt-5" animation="border" role="status">
							</Spinner>
						</Container>
					: <ProductCarousel productProps={albums}/>
				}	
			</div>
			<div id="our-process">
			</div>

		</>
	)
}
