import { useContext, useEffect, useState } from 'react';
import { Container, Card, Button, Col, Row, InputGroup, FormControl } from 'react-bootstrap';
import { Link, useParams, useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import NumberFormat from 'react-number-format';


export default function SpecificProduct() {
	const { user } = useContext(UserContext);
	const { productId } = useParams();
	const history = useHistory();

	const [ cart, setCart ] = useState([])
	const [ albumName, setAlbumName ] = useState('');
	const [ artist, setArtist ] = useState('');
	const [ image, setImage ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState(0);
	const [ quantity, setQuantity ] = useState(0);
	const [ qty, setQty ] = useState(1);
	const [ id, setId ] = useState('')



	useEffect((e) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			let productInfo = data.map(product => {
				setAlbumName(product.albumName)
				setArtist(product.artist)
				setDescription(product.description)
				setPrice(product.price)
				setQuantity(product.quantity)
				setImage(product.image)

				console.log(product.image)
			})
			// setName(data.productName);
			// setDescription(data.description);
			// setPrice(data.price);
			// setQuantity(data.quantity);
		})
	}, [])
  
  	useEffect(() => {
  		if(localStorage.getItem('cart')){
  			setCart(JSON.parse(localStorage.getItem('cart')))
  		}
  	}, [])


	const addToCart = (e) => {
		e.preventDefault();

		let alreadyInCart = false
		let productIndex;
		let cart = []

		if(localStorage.getItem('cart')){
			cart = JSON.parse(localStorage.getItem('cart'))
		}

		if(cart.length === 0){
			cart.push({
				'productId': productId,
				'albumName': albumName,
				'artist': artist,
				'image': image,
				'price': price,
				'quantity': qty,
				'subTotal': price * qty
			})

			setQuantity(quantity - 	qty)

			Swal.fire({
				title: 'Added to Cart',
				icon: 'success',
			})
		}else{
			for(let i = 0; i < cart.length; i++){
				if(cart[i].productId === productId){
					alreadyInCart = true
					productIndex = i
				}
			}
		}

		if(alreadyInCart){
			cart[productIndex].quantity += qty
			cart[productIndex].subTotal = cart[productIndex].price * cart[productIndex].quantity

			setQuantity(quantity - qty)

			Swal.fire({
				title: 'Added to Cart',
				icon: 'success',
			})

		}else{
			cart.push({
				'productId': productId,
				'albumName': albumName,
				'artist': artist,
				'image': image,
				'price': price,
				'quantity': qty,
				'subTotal': price * qty
			})
			setQuantity(quantity - qty)
			
			Swal.fire({
				title: 'Added to Cart',
				icon: 'success',
			})
		}

		localStorage.setItem('cart', JSON.stringify(cart))
	}
	
	return(
		<Row className="m-4 text-center">
			<Col>
				<img src={`${process.env.REACT_APP_API_URL}/images/${image}`} 
					alt="..." 
					id="spec-album-photo" 
					className="m-3"
				/>
			</Col>



			<Col>
				<Row className="mt-2">
					<h2>{albumName}</h2>
				</Row>
				<Row>
					<h4>{artist}</h4>	
				</Row>
				
				<hr/>

				<p>{description}</p>
				<h4 className="text-center mt-5">
					<NumberFormat 
		            value={price}
		            displayType="text"
		            thousandSeparator={true}
		            prefix="₱"/>
				</h4>
				<div className="text-center">
					{(qty === 1)
						? <input type="button" value="-" className="button-minus" onClick={(e) => setQty(qty - 1)} disabled/>
						: <input type="button" value="-" className="button-minus" onClick={(e) => setQty(qty - 1)}/>
					}
					<input type="number" className="text-center"  value={qty} readOnly/>
					{(qty < `${quantity}`)
						? <input type="button" value="+" className="button-plus" onClick={(e) => setQty(qty + 1)}/>
						: <input type="button" value="+" className="button-plus" onClick={(e) => setQty(qty + 1)} disabled/>
					}
					
				</div>
				
				{(user.isAdmin === false)
					? <Link className="btn btn-primary mt-4" onClick={addToCart}>Add to Cart</Link>
					: <Link className="btn btn-danger mt-4" to="/login">Log in to Add to Cart</Link>
				}
			</Col>
			
		</Row>
	)


}