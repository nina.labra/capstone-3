import { useState, useEffect, useContext } from 'react';
import { Redirect, useHistory } from 'react-router-dom';
import { Card, Form, Button, Container, Row } from 'react-bootstrap'; 
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login() {
	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ loginButton, setLoginButton ] = useState(false);
	const history = useHistory();

	const { user, setUser } = useContext(UserContext);

	function loginUser(e) {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			}, 
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access)
				
				retrieveUserDetails(data.access)

				Swal.fire({
					title: 'Groovy!',
					icon: 'success',
					text: 'You have successfully logged in.'
				})
				
				history.push('/')
			}else{
				Swal.fire({
					title: 'Oops',
					icon: 'error',
					text: 'Check your log in details and try again.'
				})
			}
		})
	}


	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/user/profile`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() => {
		if(email !== '' && password !== ''){
			setLoginButton(true)
		}else{
			setLoginButton(false)
		}
	}, [email, password])

	if(user.id !== null){
		return <Redirect to="/"/>
	}

	return(
		<Container id="login-container">
			<Card as={Row} className="login-card" id="login-card">
			  <Card.Header as="h5" className="login-header">Log in</Card.Header>
			  <Card.Body>
			  	<Form onSubmit={(e) => loginUser(e)}>
				  	<Form.Group>
				  		<Form.Label>Email:</Form.Label>
				  		<Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required/>
				  	</Form.Group>
				  	<Form.Group>
				  		<Form.Label>Password:</Form.Label>
				  		<Form.Control type="password" placeholder="Enter passsword" value={password} onChange={(e) => setPassword(e.target.value)}/>
				  	</Form.Group>
				  	{loginButton
				  	? <Button variant="primary" type="submit">Log in</Button>
				  	: <Button variant="primary" type="submit" disabled>Log in</Button>
				  	}
			  	</Form>

			  </Card.Body>
			</Card>
		</Container>
	)
}