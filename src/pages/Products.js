import { useState, useEffect, useContext } from 'react';
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';
import UserContext from '../UserContext';
import { Container, Card } from 'react-bootstrap';

export default function Products() {
	const { user } = useContext(UserContext);

	const [ products, setProducts ] = useState([]);

	const fetchData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(res => res.json())
		.then(data => {
			setProducts(data)
		})
	}

	useEffect((e) => {
		fetchData();
	}, [])

	return(
		<Container className="mt-5">
			<Container className="d-flex ml-3" id="products-container">
				{(user.isAdmin === true)
					? <AdminView productsData={products} fetchData={fetchData}/>
					: <UserView productsData={products}/>
				}
			</Container>		
		</Container>
	)


}