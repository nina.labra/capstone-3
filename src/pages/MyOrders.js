import { Container, Table } from 'react-bootstrap';
import { useEffect, useState } from 'react';

export default function MyOrders() {
	const [ userId, setUserId ] = useState('');
	const [ orders, setOrders ] = useState([]);

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/user/profile`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}  
	    })
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setUserId(data._id)
			setOrders(data.orders)
		})

	}, [])


	useEffect(() => {
		let ordersData = orders.map(order => {
			console.log(order)
		})
	}, [orders])

	return(
		<Container id="orders-container">
			<Container className="text-center mt-4 mb-5">
				<h2 className="m-4">My Orders</h2>
				<Table striped bordered hover>
					<thead>
						<tr>
							<th>Order ID</th>
							<th>Created On</th>
						</tr>
					</thead>
					<tbody>
						{orders.map((order) => {
							return(
								<tr>
									<td>{order._id}</td>
									<td>{order.createdOn}</td>
								</tr>
							)
						})}
					</tbody>
				</Table>
				
			</Container>
		</Container>
	)
}