import { useState, useEffect, useContext } from 'react';
import { Button, Form, Card, Col, Container } from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Register() {
	const [ firstName, setFirstName ] = useState('');
	const [ lastName, setLastName ] = useState('');
	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ verifyPassword, setVerifyPassword ] = useState('');
	const [ mobileNumber, setMobileNumber ] = useState('');
	const [ street1, setStreet1 ] = useState('');
	const [ city, setCity ] = useState('');
	const [ province, setProvince ] = useState('');
	const [ zipCode, setZipCode ] = useState('');
	const [ registerButton, setRegisterButton ] = useState(false);

	const { user } = useContext(UserContext);
	const history = useHistory();

	function registerUser(e) {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/user/checkEmail`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true){
				Swal.fire({
					title: 'Email already in use.',
					icon: 'error',
					text: 'Duplicate email found. Please try another email address.'
				})
			}else{
				fetch(`${process.env.REACT_APP_API_URL}/user/register`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password,
						mobileNumber: mobileNumber,
						address: {
							street1:  street1,
							city: city,
							province: province,
							zipCode: zipCode
						}
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					if(data === true){
						
						Swal.fire({
							title: 'Groovy!',
							icon: 'success',
							text: 'You have successfully registered.'
						})

						history.push('/login')
					}else{
						Swal.fire({
							title: 'Something went wrong',
							icon: 'error',
							text: 'Please check the fields.'
						})
					}
				})
			}
		})
	}

	useEffect(() => {
		if((email !== '' && password !== '' && verifyPassword !== '') && (password === verifyPassword)){
			setRegisterButton(true)
		}else{
			setRegisterButton(false);
		}
	}, [email, password, verifyPassword])

	if(user.id != null){
		return <Redirect to="/"/>
	}


	return(
		<Container>
			<Card className="m-4" id="register-card">
				<Card.Header as="h5" className="text-center bg-white"><h3>Register</h3></Card.Header>
				<Card.Body>
					<Form onSubmit={(e) => registerUser(e)}>
						<Form.Row>
						  <Form.Group as={Col} controlId="formGridEmail">
						    <Form.Label>First Name</Form.Label>
						    <Form.Control type="firstName" placeholder="Enter first name" value={firstName}onChange={(e) => setFirstName(e.target.value)} required />
						  </Form.Group>

						  <Form.Group as={Col} controlId="formGridPassword">
						    <Form.Label>Last Name</Form.Label>
						    <Form.Control type="lastName" placeholder="Enter last name" value={lastName} onChange={(e) => setLastName(e.target.value)} required/>
						  </Form.Group>
						</Form.Row>

						<Form.Row>
							<Form.Group as={Col} controlId="formGridEmail">
							    <Form.Label>Email</Form.Label>
							    <Form.Control type="email" value={email}placeholder="Enter email" onChange={(e) => setEmail(e.target.value)} required/>
							</Form.Group>
						</Form.Row>

					

						<Form.Row>
						  	<Form.Group as={Col} controlId="formGridPassword">
						  	  <Form.Label>Password</Form.Label>
						  	  <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
						  	</Form.Group>
						  	<Form.Group as={Col} controlId="formGridPassword">
						  	  <Form.Label>Verify password</Form.Label>
						  	  <Form.Control type="password" placeholder="Verify password" value={verifyPassword} onChange={(e) => setVerifyPassword(e.target.value)} required/>
						  	</Form.Group>
						</Form.Row>

						<Form.Group>
							<Form.Label>Mobile Number</Form.Label>
							<Form.Control type="tel" placeholder="ex. 09123456789" value={mobileNumber} onChange={(e) => setMobileNumber(e.target.value)}required/>
						</Form.Group>



						<Form.Group controlId="formGridAddress1">
						    <Form.Label>Address</Form.Label>
						    <Form.Control placeholder="1234 Main St" value={street1} onChange={(e) => setStreet1(e.target.value)} required/>
						</Form.Group>

{/*						<Form.Group controlId="formGridAddress2">
						    <Form.Label>Address 2</Form.Label>
						    <Form.Control placeholder="Apartment, studio, or floor" onChange={(e) => setStreet2(e.target.value)}/>
						</Form.Group>*/}

						<Form.Row>
						  	<Form.Group as={Col} controlId="formGridState">
						      <Form.Label>Province</Form.Label>
						      <Form.Control 
						      		as="select" 
						      		defaultValue="Choose..." 
						      		value={province} 
						      		onChange={(e) => setProvince(e.target.value)}
						      		required
						      	>
						        <option>Choose...</option>
						        <option value="Metro Manila">Metro Manila</option>
						        <option value="Abra">Abra</option>
						        <option value="Agusan del Norte">Agusan del Norte</option>
						        <option value="Agusan del Sur">Agusan del Sur</option>
						        <option value="Aklan">Aklan</option>
						        <option value="Albay">Albay</option>
						        <option value="Antique">Antique</option>
						        <option value="Apayao">Apayao</option>
						        <option value="Aurora">Aurora</option>
						        <option value="Basilan">Basilan</option>
						        <option value="Batanes">Batanes</option>
						        <option value="Batangas">Batangas</option>
						        <option value="Benguet">Benguet</option>
						        <option value="Biliran">Biliran</option>
						        <option value="Bohol">Bohol</option>
						        <option value="Cagayan">Cagayan</option>
						        <option value="Camarines Sur">Camarines Sur</option>
						        <option value="Camiguin">Camiguin</option>
						        <option value="Cebu">Cebu</option>
						        <option value="Cavite">Cavite</option>
						        <option value="Davao de Oro">Davao de Oro</option>
						        <option value="Davao del Norte">Davao del Norte</option>
						        <option value="Davao del Sur">Davao del Sur</option>
						        <option value="Davao Occidental">Davao Occidental</option>
						        <option value="Eastern Samar">Eastern Samar</option>
						        <option value="Ilocos Norte">Ilocos Norte</option>
						        <option value="Iloilo">Iloilo</option>
						        <option value="Isabela">Isabela</option>
						        <option value="Ilocos Norte">Ilocos Norte</option>
						        <option value="La Union">La Union</option>
						        <option value="Laguna">Laguna</option>
						        <option value="Lanao del Norte">Lanao del Norte</option>
						        <option value="Leyte">Leyte</option>
						      </Form.Control>
							</Form.Group>
						</Form.Row>

						<Form.Row>
						    <Form.Group as={Col} controlId="formGridCity">
						      <Form.Label>City</Form.Label>
						      <Form.Control value={city} onChange={(e) => setCity(e.target.value)}/>
						    </Form.Group>

	
						<Form.Group as={Col} controlId="formGridZip">
						   	<Form.Label>Zip Code</Form.Label>
						    <Form.Control maxLength={4} value={zipCode} onChange={(e) => setZipCode(e.target.value)}required />
						    </Form.Group>
						</Form.Row>

						<Form.Group id="formGridCheckbox">
						    <Form.Check type="checkbox" label="I agree to Grooves + Ground's Terms and Conditions" required/>
						</Form.Group>

						{registerButton
						? <Button variant="primary" type="submit">Submit</Button>
						: <Button variant="primary" type="submit" disabled>Submit</Button>
						}
					</Form>
				</Card.Body>
			</Card>
		</Container>
		
	)
}