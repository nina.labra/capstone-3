import { useState, useEffect } from 'react';
import { Container, Table, Row } from 'react-bootstrap';

export default function MyOrders() {
	const [ userId, setUserId ] = useState('');
	const [ orders, setOrders ] = useState([]);

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/orders/all-orders`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			} 
	    })
		.then(res => res.json())
		.then(data => {
			setOrders(data)
		})

	}, [])



	return(
		<Container id="orders-container">
			<Container className="text-center mt-4 mb-5">
				<h2 className="m-4">Orders</h2>
				<Table striped bordered hover>
					<thead>
						<tr>
							<th>Customer Name</th>
							<th>Items</th>
							<th>Total Amount</th>
							<th>Order Status</th>
						</tr>
					</thead>
					<tbody>
						{orders.map((order) => {
							const { items, fullName, orderStatus, totalAmount } = order;
 							return(
								<tr>
									<td>{fullName}</td>
									<td className="">{items.map(item => {
										return (
											<Row className="m-1">
												{item.albumName}
											</Row>
										)
									})}
									</td>
									<td>{totalAmount}</td>
									<td>{orderStatus}</td>
								</tr>
							)
						})}
					</tbody>
				</Table>
				
			</Container>
		</Container>
	)
}