import { Container, Table, Col, Row, Form, Button } from 'react-bootstrap';
import { useEffect, useState, input } from 'react';
import { useHistory } from 'react-router-dom';
import NumberFormat from 'react-number-format';
import Swal from 'sweetalert2';


export default function Checkout() {
	const [ cart, setCart ] = useState([]);
	const [ order, setOrder ] = useState([])
	const [ total, setTotal ] = useState(0);

	const [ qty, setQty ] = useState(1);
	const [ name, setName ] = useState('');
	const [ price, setPrice ] = useState(0);
	const [ subTotal, setSubTotal ] = useState(0);
	const [ id, setId ] = useState('');
	const [ itemQtyTotal, setItemQtyTotal ] = useState(0);

	const [ userId, setUserId ] = useState('');
	const [ firstName, setFirstName ] = useState('');
	const [ lastName, setLastName ] = useState('');
	const [ fullName, setFullName ] = useState('');
	const [ city, setCity ] = useState('');
	const [ mobileNum, setMobileNum ] = useState('')
	const [ street1, setStreet1 ] = useState('');
	const [ province, setProvince ] = useState('');
	const [ zipCode, setZipCode ] = useState(0);
	const [ modeOfPayment, setModeOfPayment ] = useState('');
	const [ orderId, setOrderId ] = useState('')
	const history = useHistory();

	useEffect(() => {
		if(localStorage.getItem('cart')){
			setCart(JSON.parse(localStorage.getItem('cart')))
		}

		fetch(`${process.env.REACT_APP_API_URL}/user/profile`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}  
	    })
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setUserId(data._id)
			setFullName(`${data.firstName} ${data.lastName}`)
			setCity(data.address.city)
			setProvince(data.address.province)
			setStreet1(data.address.street1)
			setZipCode(data.address.zipCode)
			setMobileNum(data.mobileNumber)
		})

	}, [])

	useEffect(() => {
		let tempTotal = 0;
		let tempItemQtyTotal = 0;

		let cartArr = cart.slice(1, cart.length)

		cartArr.forEach((item) => {
			tempTotal += item.price * item.quantity
			tempItemQtyTotal += item.quantity
		})

		setTotal(tempTotal)
		setItemQtyTotal(tempItemQtyTotal)
	}, [cart])



	// const getUserInfo = (e) => {
	// 	e.preventDefault();

	
	// }

	const createOrder = (e) => {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/orders/create-order`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`, 
				'Content-Type': 'application/json'
			}, 
			body: JSON.stringify({
				userId: userId,
				fullName: fullName,
				shippingAddress: {
					street1: street1,
					city: city,
					province: province,
					zipCode: zipCode
				}, 
				items: cart.slice(1, cart.length),
				totalAmount: total,
				modeOfPayment: modeOfPayment
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data._id !== undefined){

				setOrderId(data._id)

				fetch(`${process.env.REACT_APP_API_URL}/orders/sync-order`, {
					method: 'POST',
					headers: {
						Authorization: `Bearer ${localStorage.getItem('token')}`
					},
					body: {
						userId: userId,
						orderId: orderId,
						items: cart.slice(1, cart.length)
					}
				})
				.then(res => res.json())
				.then(data => {
					if(data === true){
						Swal.fire({
							title: 'Groovy!',
							icon: 'success',
							text: 'Your order has been placed.'
						})

						history.push('/my-orders');

						localStorage.removeItem('cart');
					}else{
						Swal.fire({
							title: 'Oops!',
							icon: 'error',
							text: 'Something went wrong. Please try again.'
						})
					}
					
				})
			}else{
				Swal.fire({
					title: 'Oops!',
					icon: 'error',
					text: 'Something went wrong. Please try again.'
				})
			}
		})
	}

	return(
		<>
			<Container className="text-center mt-4 mb-4">
				<h2>Order Details</h2>
			</Container>
			<Container id="order-info" className="mb-4">
				<Table 
					className="text-center" 
					as={Col}
				>
					<thead className="border-dark border-bottom">
						<tr id="table-header">
							<th>Product</th>
							<th>Price</th>
{/*							<th>Quantity</th>*/}
							<th>Item Subtotal</th>
						</tr>
					</thead>
					<tbody>
						{cart.slice(1, cart.length).map((item) => {
							if(item.quantity > 0){
								return(
									<tr>
										<td>
											<Row className="d-flex">
												<Col>
													<img 
														src={`${process.env.REACT_APP_API_URL}/images/${item.image}`} 
														id="album-photo"
													/>
												</Col>
												<Col className="mr-4 mt-2">
													<h5>{item.albumName} - {item.artist} (x{item.quantity})</h5>						
												</Col>		
											</Row>
										</td>
										<td><NumberFormat 
									            value={item.price}
									            displayType="text"
									            thousandSeparator={true}
									            prefix="₱"/>
									    </td>
										{/*<td>
											{item.quantity}
										</td>*/}
										<td><NumberFormat 
									            value={item.quantity * item.price}
									            displayType="text"
									            thousandSeparator={true}
									            prefix="₱"/>
									    </td>
									</tr>
								)
							}
						})}
					</tbody>
				</Table>
				<Row id="order-total" className="text-center">
					<span>
						<h2 className="text-center">
							Total: 
							<NumberFormat 
					            value={total}
					            displayType="text"
					            thousandSeparator={true}
					            prefix=" ₱"
					        />
						</h2>
					</span>
					
				</Row>
				<Row className="d-flex space-around">
					<Col className="m-5 p-4 border border-dark" id="order-info" as={Col}>
						<h3 className="mb-3"> Shipping Details</h3>
						<h5>{fullName}</h5>
						<p>{mobileNum} </p>
						<p>{street1}, {city}, {province}, {zipCode}</p>
					</Col>
					<Col id="order-info" className="m-5 p-4">
						<h3>Mode of Payment</h3>
							<Form as="radio" onChange={(e) => setModeOfPayment(e.target.value)}>
								<Form.Check
								    type="radio"
								    label="Cash on Delivery (COD)"
								    value="Cash on Delivery (COD)"
								    name="MoP"
								    required
								/>
								<Form.Check
								    type="radio"
								    label="GCash"
								    value="GCash"
								    name="MoP"
								    required
								/>
								<Form.Check
								    type="radio"
								    label="PayMaya"
								    value="PayMaya"
								    name="MoP"
								    required
								/>
							</Form>

					</Col>
				</Row>

				<Row>
					<Container className="m-3">
						<Button className="btn-success btn-block" onClick={createOrder}>Place Order</Button>
					</Container>
				</Row>
				

				
			</Container>
			
		</>

	)


}