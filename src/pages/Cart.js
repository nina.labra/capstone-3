import { useEffect, useState } from 'react';
import { Container, Table, Button, Card, Col, Row, InputGroup, FormControl } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import NumberFormat from 'react-number-format';

export default function MyCart() {
	const [ total, setTotal ] = useState(0);
	const [ cart, setCart ] = useState([]);

	const [ qty, setQty ] = useState(1);
	const [ name, setName ] = useState('');
	const [ price, setPrice ] = useState(0);
	const [ subTotal, setSubTotal ] = useState(0);
	const [ id, setId ] = useState('');
	const [ itemQtyTotal, setItemQtyTotal ] = useState(0);
	const history = useHistory();


	useEffect(() => {
		if(localStorage.getItem('cart')){
			setCart(JSON.parse(localStorage.getItem('cart')))
		}

	}, [])

	useEffect(() => {
		let tempTotal = 0;
		let tempItemQtyTotal = 0;

		let cartArr = cart.slice(1, cart.length)

		console.log(cartArr[0])

		cartArr.map((item) => {
			if(item._id !== null){
				tempTotal += item.price * item.quantity
				tempItemQtyTotal += item.quantity
			}

			console.log(item.quantity)

		})

		setTotal(tempTotal)
		setItemQtyTotal(tempItemQtyTotal)
	}, [cart])

	// useEffect(() => {
	// 	let cartArr = localStorage.getItem('cart')

	// 	if(cartArr.length === 1){
 //  			setCart(localStorage.removeItem('cart'))
	// 	}

	// }, [cart])

	
	const qtyInput = (productId, value) => {
		let tempCart = [...cart]

		for(let i = 1; i < tempCart.length - 1; i++){
			if(tempCart[i].productId === productId){
				tempCart[i].quantity = parseFloat(value)
				tempCart[i].subTotal = tempCart[i].price * tempCart[i].quantity
			}
		}
		setCart(tempCart)
		localStorage.setItem('cart', JSON.stringify(tempCart))
	}


	const removeItem = (productId) => {
		let tempCart = [...cart]
		let cartArr = localStorage.getItem('cart');

		for(let i = 0; i < tempCart.length; i++){
			if(tempCart[i].productId === productId){
				tempCart.splice(i, 1)

			}
		}	

		setCart(tempCart)
		localStorage.setItem('cart', JSON.stringify(tempCart))	
	}

	return(
		<Container className="mb-5">
			<h3 className="text-center mt-5">Your Shopping Cart</h3>
			<Row className="d-flex">
				<Container className="text-center mt-4 d-flex">
					{cart.length === 0
					? 	<Container className="mt-5">
							<h5>Cart is empty. </h5>
						</Container>
					: 	<Table className="mt-4" bordered as={Col}>
							<thead>
								<tr>
									<th>Product</th>
									<th>Price</th>
									<th>Quantity</th>
									<th>Item Subtotal</th>
								</tr>
							</thead>
							<tbody>
								{cart.slice(1, cart.length).map((item) => {
									if(item.quantity > 0){
										return(
											<tr>
												<td>
													<Row>
														<Col>
															<img 
																src={`${process.env.REACT_APP_API_URL}/images/${item.image}`} 
																id="album-photo"
															/>
														</Col>
														<Col className="d-flex mr-4 mt-2">
															{item.albumName} - {item.artist}
														</Col>		
													</Row>
												</td>
												<td><NumberFormat 
											            value={item.price}
											            displayType="text"
											            thousandSeparator={true}
											            prefix="₱"/>
											    </td>
												<td style={{width: '10rem'}}>
													<InputGroup>
														{item.quantity === 1
															?   <Button variant="outline-secondary" id="button-addon1" onClick={(e) => qtyInput(item.productId, item.quantity-= 1)} disabled>
													     		-
													   			</Button>
													   		: <Button variant="outline-secondary" id="button-addon1" onClick={(e) => qtyInput(item.productId, item.quantity-= 1)}>
													     		-
													   			</Button>
														}
													   <FormControl className="text-center" style={{width: '2rem'}} value={item.quantity }
													     aria-describedby="basic-addon1"
													   readOnly />
													   <Button variant="outline-secondary" id="button-addon2" onClick={(e) => qtyInput(item.productId, item.quantity += 1)}>
													         +
													       </Button>
													 </InputGroup>
													 <Button className="btn btn-danger mt-2" onClick={(e) => removeItem(item.productId)}>Remove Item</Button>
												</td>
												<td><NumberFormat 
											            value={item.price * item.quantity}
											            displayType="text"
											            thousandSeparator={true}
											            prefix="₱"/>
											    </td>
											</tr>
										)
									}
								})}
							</tbody>
						</Table>

					}
					<Card className="mt-4 ml-5"style={{width: '12rem'}, {height: '14rem'}}>
						<Card.Header className="align-center">
							<h5>Subtotal ({itemQtyTotal} items):</h5> 
						</Card.Header>
						<Card.Body>
							<Card.Text><NumberFormat 
						            value={total}
						            displayType="text"
						            thousandSeparator={true}
						            prefix="₱"/>
						    </Card.Text>
					    </Card.Body>
					    <Card.Footer>
					    	<Link className="btn btn-primary" to='/checkout'>Proceed to Checkout</Link>
					    </Card.Footer>
					</Card>
				</Container>
			</Row>

		</Container>
		
	)
}