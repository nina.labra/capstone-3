import { useState, useEffect } from 'react';
import Product from './Product';
import { Row, Container } from 'react-bootstrap';

export default function UserView({productsData}){
	const [ products, setProducts ] = useState('');

	useEffect((e) => {


		const productsArr = productsData.map(product => {
			if(product.isActive === true){
				return(
					<Product productsData={product} key={product._id}/>
				)
			}else{
				return null;
			}
		})
		
		// const productsArr = productsData.map(product => {
		// 	if(product.isActive === true){

				
		// 	}else{
		// 		return null;
		// 	}
		// })

		setProducts(productsArr)

	}, [productsData])

	return(

		<Container className="text-center">
			<h1>Shop</h1>		
			<Row>
				{products}
			</Row>
		</Container>
	)
}