import { Navbar, Nav } from 'react-bootstrap';
import { useContext, useEffect } from 'react';
import UserContext from '../UserContext';
import { FaShoppingCart } from 'react-icons/fa'
import { Link, useHistory } from 'react-router-dom';
import logo from '../navbarlogo.png'

export default function NavBar(){
	const { user, unsetUser } = useContext(UserContext);
	const history = useHistory();

	const logout = () => {
		unsetUser()
		history.push('/login')
	}

	let rightNav = (localStorage.getItem('token') === null) ? (
		<>
			<Link className="nav-link" to="/register">Register</Link>
			<Link className="nav-link" to="/login">Log In</Link>
		</>
	) : (
		<>
			{user.isAdmin === false 
				? <Link className="nav-link" to="my-orders">My Orders</Link>

				: <Link className="nav-link" to="orders">Orders</Link>
			}
			<Nav.Link onClick={logout}>Log Out</Nav.Link>	
		</>
	)



	return(
		<>
		  <Navbar variant="light" expand="lg" id="navbar">
		    <Navbar.Brand id="navbarbrand">
				<img
					src={logo}
					width="30"
					height="30"
					className="d-inline-block align-top"
				/>{' '}
				The Groove Grove
			</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      <Nav className="mr-auto">
		        <Nav.Link href="/">Home</Nav.Link>
		        <Nav.Link href="/products">Shop</Nav.Link>
		        {rightNav}
		      </Nav>
		    </Navbar.Collapse>
		    {user.isAdmin === false
		    	? <Link id="cart-icon" className="nav-link" to="/cart"><FaShoppingCart className="align-center"/></Link>
		    	: <></>
		    }
		  </Navbar>
		</>
	)
}