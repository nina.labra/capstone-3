import { useState, useEffect } from 'react';
import { Container, Button, Modal, Form, Col, Table } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useHistory, Link } from 'react-router-dom';
import NumberFormat from 'react-number-format';
import Axios from 'axios'

export default function AdminView(props){
	const { productsData, fetchData } = props;

	const [products, setProducts] = useState([]);
	const [ productId, setProductId ] = useState('');
	const [ showAdd, setShowAdd ] = useState(false);
	const [ showEdit, setShowEdit ] = useState(false);
	const [ showEditPhoto, setShowEditPhoto ] = useState(false);
	const [ albumName, setAlbumName ] = useState('');
	const [ artist, setArtist ] = useState('');
	const [ genre, setGenre ] = useState('');
	const [ image, setImage ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState('');
	const [ quantity, setQuantity ] = useState('');

	const history = useHistory();


	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);

	const openEdit = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			let productInfo = data.map(info => {
				setProductId(info._id)
				setAlbumName(info.albumName)
				setArtist(info.artist)
				setGenre(info.genre)
				setQuantity(info.quantity)
				setDescription(info.description)
				setPrice(info.price)
			})	
		})

		setShowEdit(true)
	}

	const closeEdit = () => {
		setShowEdit(false)
		setAlbumName("")
		setArtist("")
		setGenre("")
		setDescription("")
		setPrice(0)
	}

	const openEditPhoto = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			let albumInfo = data.map((album) => {
				setImage(album.image)
				setProductId(album._id)
			})		
		})

		setShowEditPhoto(true)
	}

	const closeEditPhoto = () => {
		setShowEditPhoto(false)
	}

	useEffect(() => {
		const productsArr = productsData.map(product => {
			return (
				<tr>
					<td>
						<img 
							src={`${process.env.REACT_APP_API_URL}/images/${product.image}`} 
							id="admin-album-photo" 
							alt="..."
							className="mb-3"
						/>
						<Link onClick={(productId) => openEditPhoto(product._id)}>Change photo</Link>
					</td>
					<td>{product.albumName}</td>
					<td>{product.artist}</td>
					<td>{product.description}</td>
					<td><NumberFormat 
							value={product.price}
			            	displayType="text"
			           		thousandSeparator={true}
			            	prefix="₱"
						/>
					</td>
					<td>{product.quantity}</td>
					<td>
						{product.isActive
							? <p>Available</p>
							: <p>Unavailable</p>
						}
					</td>
					<td>
						<Button className="m-3" as={Col} variant="primary" size="m" onClick={() => openEdit(product._id)}>
						Update
						</Button>
						{product.isActive
							? <Button className="m-3" as={Col} variant="danger" size="m" onClick={(e) => archiveToggle(product._id, product.isActive)}>Disable</Button>
							: <Button className="m-3" as={Col} variant="success" size="m" onClick={() => archiveToggle(product._id, product.isActive)}>Enable</Button>
						}
					</td>
				</tr>
			)
			
		})
		setProducts(productsArr)
	}, [productsData])

	const addProduct = (e) => {
		e.preventDefault();

		const formData = new FormData();
	
		formData.append("albumName", albumName)
		formData.append("artist", artist)
		formData.append("genre", genre)
		formData.append("description", description)
		formData.append("price", price)
		formData.append("quantity", quantity)
		formData.append("isActive", true)
		formData.append("image", image)

		Axios.post(`${process.env.REACT_APP_API_URL}/products`, formData, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res)
		.then(data => {
			console.log(data)
			if(data.data === true){
				fetchData()

				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product successfully added'
				})

				setAlbumName("")
				setDescription("")
				setArtist("")
				setPrice("")
				setGenre("")
				setImage(null)
				setQuantity("")

				closeAdd()

				history.push("/products")
			}else{
				fetchData()

				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again'
				})

			}
		})
	}

	const editProduct = (e, productId) => {
		e.preventDefault();


		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				albumName: albumName,
				artist: artist,
				genre: genre,
				description: description,
				price: price,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				fetchData()
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product successfully updated'
				})

				setAlbumName("")
				setArtist("")
				setGenre("")
				setDescription("")
				setPrice(0)
				setQuantity(0)
				closeAdd()
			}else{
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again'
				})
			}
		})
	}

	const editAlbumPhoto = (e, productId) => {
		e.preventDefault();

		// fetch(`${process.env.REACT_APP_API_URL}/products/images/${productId}`, {
		// 		method: 'PUT',
		// 		headers: {
		// 			Authorization: `Bearer ${localStorage.getItem('token')}`
		// 		},
		// 		file: JSON.stringify ({
		// 			filename: image
		// 		})
		// })
		// .then(res => res.json())
		// .then(data => {
		// 	console.log(data)
		// })

		const updatedPhoto = {
			image: image
		}

		Axios.put(`${process.env.REACT_APP_API_URL}/products/images/${productId}`, updatedPhoto, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'multipart/form-data'
			}
		})
		.then(res => console.log(res))
		.then(data => {
			console.log(data)
		})
		// .then(res => console.log(res.json())
		// .then(data => {
		// 	if(data.data === true){
		// 		console.log(data)
		// 	}
		// })
	}

	const archiveToggle = (productId, isActive) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				fetchData()
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product successfully archived / unarchived'
				})
			}else{
				fetchData()
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again'
				})
			}
		})
	}

	return(
		<Container className="text-center">
		{/*Products Array*/}

			<div className="text-center mb-3">
				<h2>Admin Dashboard</h2>
				<Button variant="primary" onClick={openAdd}>Add New Product</Button>
			</div>

			<Table responsive="md">
				<thead>
					<tr>
						<th>Album Photo</th>
						<th>Album Name</th>
						<th>Artist</th>
						<th>Description</th>
						<th>Price</th>
						<th>Stock</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{products}
				</tbody>
			</Table>

		{/*Edit Modal*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={(e) => editProduct(e, productId)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Album Name</Form.Label>
							<Form.Control 
								type="text"
								value={albumName}
								placeholder={albumName}
								onChange={(e) => setAlbumName(e.target.value)} 
								required
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Artist</Form.Label>
							<Form.Control 
								type="text"
								value={artist}
								placeholder={artist}
								onChange={(e) => setArtist(e.target.value)}
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Genre</Form.Label>
							<Form.Control
								type="text"
								value={genre}
								onChange={(e) => setGenre(e.target.value)}
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control 
								as="textarea" 
								value={description} 
								onChange={(e) => setDescription(e.target.value)}
							/>
						</Form.Group>
						<Form.Row>
							<Form.Group as={Col}>
								<Form.Label>Price</Form.Label>
								<Form.Control 
									type="number"
									value={price} 
									onChange={(e) => setPrice(e.target.value)}
									/>
							</Form.Group>

							<Form.Group as={Col}>
								<Form.Label>Stock</Form.Label>
								<Form.Control 
									type="number" 
									value={quantity} 
									onChange={(e) => setQuantity(e.target.value)}
								/>
							</Form.Group>
						</Form.Row>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit" onClick={(e) => editProduct(e, productId)}>Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		{/*Change album pic Modal*/}
			<Modal show={showEditPhoto} onHide={closeEditPhoto}>
				<Modal.Header closeButton>
					<Modal.Title>Change Album Photo</Modal.Title>
				</Modal.Header>
				<Form enctype="form-data" onSubmit={(e, productId) => editAlbumPhoto(productId)}>
						<Modal.Body>
							<img 	
								src={`${process.env.REACT_APP_API_URL}/images/${image}`} 
								alt="..."
								id="admin-change-photo"
								className=""
							/>
							<Form.Group>
								<Form.Label className="mb-2">Upload Photo</Form.Label>
								<Form.Control
									type="file"
									className="form-control-file"
									onChange={(e) => setImage(e.target.files[0])}
								/>
							</Form.Group>
						</Modal.Body>

						<Modal.Footer>
							<Button 
								variant="secondary" 
								onClick={closeEditPhoto}
							>
							Close
							</Button>
							<Button onSubmit={(e) => editAlbumPhoto(productId)}>Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

		{/*Add Modal*/}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form enctype="form-data" onSubmit={(e) => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Album Name</Form.Label>
							<Form.Control 
								type="text" 
								value={albumName} 
								onChange={(e) => setAlbumName(e.target.value)} 
								required
							/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Artist</Form.Label>
							<Form.Control 
								type="text" 
								value={artist} 
								onChange={(e) => setArtist(e.target.value)} 
								required
							/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Genre</Form.Label>
							<Form.Control 
								type="text" 
								value={genre} 
								onChange={(e) => setGenre(e.target.value)} 
								required
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control 
								as="textarea" 
								value={description} 
								onChange={(e) => setDescription(e.target.value)}
							/>
						</Form.Group>
						<Form.Row>
							<Form.Group as={Col}>
								<Form.Label>Price</Form.Label>
								<Form.Control 
									type="number" 
									value={price} 
									onChange={(e) => setPrice(e.target.value)}
								/>
							</Form.Group>

							<Form.Group as={Col}>
								<Form.Label>Stock</Form.Label>
								<Form.Control 
									type="number" 
									value={quantity} 
									onChange={(e) => setQuantity(e.target.value)}
								/>
							</Form.Group>
						</Form.Row>
						<Form.Row>
							<Form.Group controlId="formFile"  className="mb-3">
								<Form.Label>Upload Photo</Form.Label>
								<input
									type="file"
									className="form-control-file" 
									onChange={(e) => setImage(e.target.files[0])}
								/>          
							</Form.Group>
						</Form.Row>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit" onClick={addProduct}>Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>


		</Container>
	)
}