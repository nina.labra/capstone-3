import { Row, Col, Container } from 'react-bootstrap';
import { IoLogoInstagram, IoLogoFacebook } from 'react-icons/io'
import { HiLocationMarker, HiOutlineMail } from 'react-icons/hi'
import { AiFillPhone } from 'react-icons/ai'
 
export default function Footer() {
	return(
		<div id="footer" className="text-center wrapper">
			<h2 className="mb-3">Contact Us</h2>
		    <Row>
		    	<Col>
		    		<HiLocationMarker size={28} className="mb-2"/>
		    		<span>
		    			<p>123 Main St. Makati City, Metro Manila, Philippines</p>
		    		</span>
		    	</Col>
		    	<Col>
		    		<AiFillPhone size={28} className="mb-2"/>
		    		<span className="text-center">
		    			<p>Mobile: +639123405789 </p>
		    		</span>
		    		<span>
		    			<p>Tel: 0223423429 </p>
		    		</span>
		    	</Col>
		    	<Col>
		    		<HiOutlineMail size={28} className="mb-4"/>
		    		<span>
		    			<p>groovegrove@mail.com</p>
		    		</span>
		    	</Col>	
		    </Row>
		    <div className="text-center m-3">
		    	<IoLogoInstagram size={28}/>
		    	<IoLogoFacebook size={28}/>
		    </div>
		    <div id="copyright-container">
		    	<p className="fluid bottom">
		    		&copy; {new Date().getFullYear()} Copyright <a href="https://groovegrove.ph">The Groove Grove</a>
		    	</p>
		    </div>
		</div>
	)
}