import { useState, useEffect } from 'react';
import { Carousel, Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCarousel({productProps}) {
	const product = productProps;
	const [loading, setLoading] = useState(true);
	const [albums, setAlbums] = useState([]);

	useEffect((e) => {
		setAlbums(productProps)
		setLoading(false)
	
		// console.log(productProps[0].image)
	
	}, [productProps])

	return(
		<div>
			<Container>
				<Carousel>
					<Carousel.Item className="bg-dark mt-4 text-light">
						<Row>
							<Col>
								<img src={`${process.env.REACT_APP_API_URL}/images/${product[1].image}`} className="m-3" id="featured-album-photo" alt="..."/>
							</Col>
							<Col className="justify-content mt-4">
								<Container>
									<h1 id="featured-title">{product[1].albumName}</h1>
									<h3>{product[1].artist}</h3>
									<Link variant="link" to={`products/${product[1]._id}`}>Buy Now</Link>
								</Container>
							</Col>
						</Row>
					</Carousel.Item>	

					<Carousel.Item className="bg-dark mt-4 text-light">
						<Row>
							<Col>
								<img src={`${process.env.REACT_APP_API_URL}/images/${product[0].image}`} className="m-3" id="featured-album-photo" alt="..."/>
							</Col>
							<Col className="justify-content mt-4">
								<Container>
									<h1 id="featured-title">{product[0].albumName}</h1>
									<h3>{product[0].artist}</h3>
									<Link variant="link" to={`products/${product[0]._id}`}>Buy Now</Link>
								</Container>
							</Col>
						</Row>
					</Carousel.Item>	

					<Carousel.Item className="bg-dark mt-4 text-light">
						<Row>
							<Col>
								<img src={`${process.env.REACT_APP_API_URL}/images/${product[2].image}`} className="m-3" id="featured-album-photo" alt="..."/>
							</Col>
							<Col className="justify-content mt-4">
								<Container>
									<h1 id="featured-title">{product[2].albumName}</h1>
									<h3>{product[2].artist}</h3>
									<Link variant="link" to={`products/${product[2]._id}`}>Buy Now</Link>
								</Container>
							</Col>
						</Row>
					</Carousel.Item>	

				</Carousel>
			</Container>
		</div>
		
	)
	
}
