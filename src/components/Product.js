import { Card, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import NumberFormat from 'react-number-format';

export default function Product({productsData}){
	const { _id, albumName, artist, price, quantity, image } = productsData;

	return(
		<div className="products">
			<Card className="m-4 text-center" id="products-cards" style={{width: '18rem'}}>
				<Card.Body>
					<img src={`${process.env.REACT_APP_API_URL}/images/${image}`} className="m-3" id="album-photo" alt="..."/>
					<Card.Title>{albumName}</Card.Title>
					<p>{artist}</p>
					<h6><NumberFormat 
			            value={price}
			            displayType="text"
			            thousandSeparator={true}
			            prefix="₱"
			            />
				    </h6>
					<Link variant="link" to={`products/${_id}`}>Details</Link>
				</Card.Body>
			</Card>		
		</div>
	)
}