import { Row, Col, Jumbotron } from 'react-bootstrap'

export default function Banner({bannerProps}){
	const { title, subheader } = bannerProps;

	return(
		<Row>
			<Col>
				<Jumbotron className="banner text-center" id="jumbotron">
					<h1 id="jumbo-title">{title}</h1>
					<p id="jumbo-subheader">{subheader}</p>
				</Jumbotron>
			</Col>
		</Row>

	)
}