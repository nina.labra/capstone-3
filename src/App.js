import { useState, useEffect } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import NavBar from './components/NavBar';
import Footer from './components/Footer';
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import Products from './pages/Products';
import SpecificProduct from './pages/SpecificProduct';
import Cart from './pages/Cart';
import Checkout from './pages/Checkout';
import MyOrders from './pages/MyOrders';
import Orders from './pages/Orders';
import { UserProvider } from './UserContext';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

export default function App() {

  const [ user, setUser ] = useState({
    id: null,
    isAdmin: null
  })

  const [ cart, setCart ] = useState([]);

  useEffect(() => {
    if(localStorage.getItem('cart')){
      setCart(JSON.parse(localStorage.getItem('cart')))
    }
  }, [])


  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/user/profile`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
          orders: data.orders
        })
      }else{
        setUser({
          id: null, 
          isAdmin: null,
          orders: null
        })
      }
    })
  }, [])

  // useEffect(() => {
  //   if()
  //   setCart(cart)
  // }, [])

  const unsetUser = () => {
    localStorage.clear();
    setUser({
      id: null,
      isAdmin: null
    });
  }

  return (
    <UserProvider id="app" value={{cart, setCart, user, setUser, unsetUser}}>
      <Router>
      <NavBar/>
      <Switch>
        <Route exact path="/" component={Home}/>
        <Route exact path="/login" component={Login}/>
        <Route exact path="/register" component={Register}/>
        <Route exact path="/products" component={Products}/>
        <Route exact path="/products/:productId" component={SpecificProduct}/>
        <Route exact path="/cart" component={Cart}/>
        <Route exact path="/checkout" component={Checkout}/>
        <Route exact path="/my-orders" component={MyOrders}/>
        <Route exact path="/orders" component={Orders}/>
      </Switch>
      <Footer/>
      </Router>
    </UserProvider>
  );
}
